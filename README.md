Project created as minimum example to demonstrate issues with dependency scanning of Python projects.

The CI pipeline should pick up and run a dependency scan on the project.
This will process the Python packages in `requirements.txt`.
The Gemnasium container is unable to install the `psycopg2-binary` due to missing system dependencies.

See [Issue #6713](https://gitlab.com/gitlab-org/gitlab/issues/6713) for details.
And also [Issue #14958](https://gitlab.com/gitlab-org/gitlab/issues/14958)
